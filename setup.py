from setuptools import setup
setup(
    name='model_asdf',
    version="0.0.4",
    packages=['model_asdf', ],
    install_requires=["asdf", "astropy>=4.0"],
    author="Ari Takalo",
    url="https://gitlab.com/variaatio/model_asdf",
    classifiers=[
        "License :: OSI Approved :: MIT License"
    ],
    license="MIT License",
    data_files=[('schemas',['schemas/model-0.0.1.yaml',
                            'schemas/parameter-0.0.1.yaml',
                            'schemas/Moffat2D-0.0.1.yaml',
                            'schemas/boundbox-0.0.1.yaml',
                            'schemas/bound-0.0.1.yaml',
                            'schemas/Gaussian2D-0.0.1.yaml',
                            'schemas/Ellipse2D-0.0.1.yaml']), ]
)
