from asdf.types import CustomType
from asdf.yamlutil import tagged_tree_to_custom_tree, custom_tree_to_tagged_tree
from astropy.modeling import Parameter, Model, Fittable2DModel
from astropy.modeling import models as astromodels
from model_asdf.util import BoundBox, Bound, param2param


class ModelAsdfType(CustomType):
    organization = 'https://gitlab.com/variaatio'
    standard = 'custom/model'


class BoundType(ModelAsdfType):
    name = 'bound'
    types = [Bound, ]
    version = (0, 0, 1)

    @classmethod
    def to_tree(cls, node, ctx):
        tree = {}
        if isinstance(node, Bound):
            tree['a'] = node.axis
            tree['l'] = node.low
            tree['h'] = node.high

    @classmethod
    def from_tree(cls, tree, ctx):
        return Bound(tree['a'],
                     (tree['l'],
                      tree['h']))


class BoundBoxType(ModelAsdfType):
    name = 'boundbox'
    types = [BoundBox, ]
    version = (0, 0, 1)

    @classmethod
    def to_tree(cls, node, ctx):
        tree = []
        if isinstance(node, BoundBox):
            for bound in node:
                tree.append(custom_tree_to_tagged_tree(bound, ctx))
            return tree
        else:
            raise TypeError()

    @classmethod
    def from_tree(cls, tree, ctx):
        box = []
        for entry in tree:
            box.append(tagged_tree_to_custom_tree(entry, ctx))
        return BoundBox(box)


class ParameterType(ModelAsdfType):
    name = 'parameter'
    organization = 'https://gitlab.com/variaatio'
    version = (0, 0, 1)
    standard = 'custom/model'
    types = [Parameter, ]

    @classmethod
    def to_tree(cls, node, ctx):
        tree = {}
        if isinstance(node, Parameter):
            tree['name'] = node.name
            if node.unit is None:
                tree['unit'] = None
            else:
                tree['unit'] = custom_tree_to_tagged_tree(node.unit,
                                                          ctx)
            tree['fixed'] = node.fixed
            tree['min'] = node.min
            tree['max'] = node.max
            tree['value'] = node.value
            tree['prior'] = node.prior
            tree['posterior'] = node.posterior
            if node.internal_unit is None:
                tree['internal_unit'] = None
            else:
                tree['internal_unit'] = custom_tree_to_tagged_tree(node.internal_unit,
                                                                   ctx)
            return tree
        else:
            raise TypeError('must be a Parameter')

    @classmethod
    def from_tree(cls, tree, ctx):
        parameter = Parameter(name=tree.get('name', ''),
                              unit=tree.get('unit'),
                              fixed=tree.get('fixed', False),
                              min=tree.get('min'),
                              max=tree.get('max'))
        parameter.prior = tree.get('prior')
        parameter.posterior = tree.get('posterior')
        parameter.value = tree.get('value')
        parameter._internal_unit = tree.get('internal_unit')
        return parameter


class ModelType(ModelAsdfType):
    name = 'Model'
    organization = 'https://gitlab.com/variaatio'
    version = (0, 0, 1)
    types = [Model, ]

    @classmethod
    def modeltotree(cls,node,ctx):
        tree = {}
        tree['name'] = node.name
        try:
            tree['bounding_box'] = custom_tree_to_tagged_tree(node.bounding_box,
                                                              ctx)
        except NotImplementedError:
            # if not implemented ignore
            pass
        tree['fittable'] = node.fittable
        tree['meta'] = custom_tree_to_tagged_tree(node.meta, ctx)
        parameters = ModelType.parameters2tree(node, ctx)
        tree.update(parameters)
        # __len__ of model is defined by n_models
        tree['nmodels'] = len(node)
        tree['modelsetaxis'] = node.model_set_axis

        return tree

    @classmethod
    def putmodel(cls, target, tree, ctx):
        target.name = tree.get('name', None)
        target.fittable = tree.get('fittable', False)

        try:
            boundbox = tagged_tree_to_custom_tree(tree['bounding_box'], ctx)
            target.bounding_box = boundbox.inmodeltuple
        except NotImplementedError:
            # if bounding box is not implemented for this specific model
            # let it be
            pass
        target.meta = tagged_tree_to_custom_tree(tree['meta'], ctx)
        ModelType.putparameters(target, tree, ctx)

    @classmethod
    def putparameters(cls, target, tree, ctx):
        if issubclass(target, Model):
            for aname in target.param_names:
                modelsparameter = getattr(target,
                                          aname)
                treeparameter = tagged_tree_to_custom_tree(tree[aname],
                                                           ctx)
                param2param(modelsparameter,
                            treeparameter)
        else:
            raise TypeError('must be a Model subclass')

    @classmethod
    def paramstokws(cls,target,tree,ctx):
        # check model for parameter names and extract these
        # as Parameter objectst dictionary
        # also returns the fixes and bounds dicts, since those will
        # be ignored from insertion by parameter
        kws = {}
        bounds = {}
        fixed = {}
        for aname in target.param_names:
            try:
                aparam = tagged_tree_to_custom_tree(tree[aname],
                                                    ctx)
            except KeyError:
                pass
            else:
                fixed[aname] = aparam.fixed
                bounds[aname] = aparam.bounds
                kws[aname] = aparam
        kws['fixed'] = fixed
        kws['bounds'] = bounds
        return kws

    @classmethod
    def parameters2tree(cls,target,ctx):
        tree = {}
        if isinstance(target, Model):
            for aname in target.param_names:
                tree[aname] = custom_tree_to_tagged_tree(getattr(target,
                                                                 aname),
                                                         ctx)
            return tree
        else:
            raise TypeError('must be a Model instance')

    @classmethod
    def to_tree(cls, node, ctx):
        # main tree span must be implemented in subclass
        raise NotImplementedError('this class must be subclassed')

    @classmethod
    def from_tree(cls, tree, ctx):
        # main tree span must be implemented in subclass
        raise NotImplementedError('this class must be subclassed')


class Fittable2DModelType(ModelType):
    name = 'Fittable2DModel'
    version = (0, 0, 1)
    types = [Fittable2DModel, ]

    @classmethod
    def F2Dfromtree(cls,tree,ctx):
        pass

    @classmethod
    def F2D2tree(cls,node,ctx):
        tree = {}
        modelinfo = ModelType.modeltotree(node,
                                          ctx)
        tree.update(modelinfo)
        return tree


class Gaussian2DType(Fittable2DModelType):
    name = 'Gaussian2D'
    version = (0, 0, 1)
    types = [astromodels.Gaussian2D, ]

    @classmethod
    def to_tree(cls, node, ctx):
        tree = {}
        F2Dinfo = Fittable2DModelType.F2D2tree(node,
                                               ctx)
        tree.update(F2Dinfo)
        return tree

    @classmethod
    def from_tree(cls, tree, ctx):
        # main tree span must be implemented in subclass
        kwargs = ModelType.paramstokws(astromodels.Gaussian2D,
                                       tree,
                                       ctx)
        model = astromodels.Gaussian2D(**kwargs)

        return model


class Moffat2DType(Fittable2DModelType):
    name = 'Moffat2D'
    version = (0, 0, 1)
    types = [astromodels.Moffat2D, ]

    @classmethod
    def to_tree(cls, node, ctx):
        tree = {}
        F2Dinfo = Fittable2DModelType.F2D2tree(node,
                                               ctx)
        tree.update(F2Dinfo)
        return tree

    @classmethod
    def from_tree(cls, tree, ctx):
        # main tree span must be implemented in subclass
        kwargs = ModelType.paramstokws(astromodels.Moffat2D,
                                       tree,
                                       ctx)
        model = astromodels.Moffat2D(**kwargs)

        return model


class Ellipse2DType(Fittable2DModelType):
    name = 'Ellipse2D'
    version = (0, 0, 1)
    types = [astromodels.Ellipse2D, ]

    @classmethod
    def to_tree(cls, node, ctx):
        tree = {}
        F2Dinfo = Fittable2DModelType.F2D2tree(node,
                                               ctx)
        tree.update(F2Dinfo)
        return tree

    @classmethod
    def from_tree(cls, tree, ctx):
        # main tree span must be implemented in subclass
        kwargs = ModelType.paramstokws(astromodels.Ellipse2D,
                                       tree,
                                       ctx)
        model = astromodels.Ellipse2D(**kwargs)

        return model


models = [Ellipse2DType, Moffat2DType, Gaussian2DType, Fittable2DModelType,
          ModelType, ParameterType, BoundBoxType, BoundType]
