class XY(object):

    def __init__(self,*args):
        self._x = None
        self._y = None
        if len(args) == 2:
            self._x = args[0]
            self._y = args[1]
        elif len(args) == 1:
            self._x = args[0][0]
            self._y = args[0][1]
        else:
            raise ValueError

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @property
    def xy(self):
        return (self.x,self.y)

class Bound(object):
    _axis='x'

    def __init__(self, axis, double):
        self._low = double[0]
        self._high = double[1]
        self._axis=axis

    @property
    def low(self):
        return self._low

    @property
    def high(self):
        return self._high

    @property
    def axis(self):
        return self._axis

    @property
    def lowhigh(self):
        return (self.low,self.high)
class BoundBox(list):
    _order=('x','y','z')

    def __init__(self,bounds):
        if isinstance(bounds[0],(int,float)):
            #first element being number means it is (xlow,xhigh)
            _bounds = (Bound('x',(bounds[0],bounds[1])))
        elif isinstance(bounds[0],(tuple)):
            # multidimension bounding box
            _bounds=list()
            #zip axis reference as per Model spec
            for abound in zip(BoundBox._order,bounds[::-1]):
                _bounds.append(Bound(abound[0],abound[1]))
        elif isinstance(bounds[0],(Bound)):
            #single bound means it is most likely all Bound sequence
            _bounds=bounds
        super(BoundBox,self).__init__(_bounds)

    @property
    def inmodeltuple(self):
        #return in tuple type models want
        asd=dict(self)
        bounds=list()
        for akey in BoundBox._order:
            try:
                bounds.append(asd[akey])
            except KeyError:
                break
        return tuple(bounds[::-1])


def param2param(target, source):
    #assign parameters values from another source parameters directly to target
    #parameter, since parameters shun simple copy over
    #this doesn't change parameter name etc. just copies over
    # relevant per use mutable information
    if isinstance(target,Parameter) and isinstance(source.Parameter):
        target.value = source.value
        target.unit = source.unit
        target.min = source.min
        target.max = source.max
        target.prior = source.prior
        target.posterior = source.posterior
    else:
        raise TypeError('objects must be Parameters')
