from os.path import dirname, abspath
import asdf
from asdf import util
from model_asdf.type import models


class ModelExtension(asdf.AsdfExtension):
    _types = []

    @property
    def url_mapping(self):
        return [('https://gitlab.com/variaatio/schemas/custom/model/',
                 util.filepath_to_url(abspath(dirname(__file__) +
                                              '/../schemas')) +
                 '/{url_suffix}.yaml')]

    @property
    def tag_mapping(self):
        return [('tag:https://gitlab.com/variaatio:custom/model/',
                 'https://gitlab.com/variaatio/schemas/custom/model/{tag_suffix}')]

    @property
    def types(self):
        return models
